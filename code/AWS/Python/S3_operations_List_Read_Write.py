import boto3

# s3 object creation.  - 0
s3 = boto3.resource(
    's3',
    aws_access_key_id = "XXXX",
aws_secret_access_key = "XXXX"
)

# Listing Buckets.  - 1
for bucket in s3.buckets.all():
    print(bucket.name)


# Listing objects in a bucket - 2
bucket = s3.Bucket('bucket-name')

for obj in bucket.objects.all():
    print(obj.key)

# Writing contents to a new file in s3 - 3
object = s3.Object('bucket-name', 'output_file.txt')
object.put(Body="data stream")
