# Simple Python Flask Docker App for Beginners #

Build the image using the following command

```bash
command: docker build -t <repository-name>/<image-name>:<tag-name> <location> 
$ docker build -t myselfsamrat/hey-there-python-flask:0.0.1.RELEASE .
```

Run the Docker container using the command shown below.

```bash
command: docker container run -d -p 3000:3000 <repository-name>/<image-name>:<tag-name>
$ docker container run -d -p 3000:3000 myselfsamrat/hey-there-python-flask:0.0.1.RELEASE
```

The application will be accessible from browser 
`http://<host_ip>:3000`

Push the image to your Docker Hub repository:
```bash
docker login
```

```bash
command:docker push <repository-name>/<image-name>:<tag-name>
docker push myselfsamrat/hey-there-python-flask:0.0.1.RELEASE
```
