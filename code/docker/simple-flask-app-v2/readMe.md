#https://flask.palletsprojects.com/en/3.0.x/installation/#python-version
#https://tecadmin.net/how-to-create-and-run-a-flask-application-using-docker/
# Create a Basic Flask Application and then dockerize it #

## What is Flask? ##
Flask is a lightweight Python framework for building web applications. It is simple, flexible, and pragmatic. It can be easily extended with the use of extensions and plug-ins. Flask can be used to create small websites or large-scale, complex applications.
Flask is used by companies like Pinterest, Twitter, Walmart, and Cisco. One of the common uses of Flask is for REST APIs that are used to interact with other systems. Applications written in Flask can also easily be deployed to a server or a cloud.

### Let’s begin with creating a new directory: ###
mkdir flask-app && flask-app 

 ### Next, create the Python virtual environment and then activate the environment. ###
python3 -m venv venv 

source venv/bin/activate 


### Now install the Flask python module under the virtual environment. ###
pip install Flask 

### The below command will create the requirements.txt file with the installed packages under the current environment. This file is useful for installing module at deployments.  ###
pip freeze > requirements.txt 
### Now, create a sample Flask application.. You can write your code in a .py file and run it with the python command. ###
app.py 


### Your sample Flask application is ready. You can run this script locally with Python now. ###
flask run --host 0.0.0.0 --port 3000 


## Create a Dockerfile for Your Flask Application ##

### Let’s create a file named Dockerfile under the project directory. This is the file docker reads instructions to build image. ###
 Dockerfile 

### build docker image ###
docker build -t flask-app . 

### run docker image ###
sudo docker run -it -p 3000:3000 -d flask-app