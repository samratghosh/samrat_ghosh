print('_________Lets Play a game_____________')
print('Enter name of player 1')
player1 = input()
print('Enter name of Player 2')
player2 = input()


pat ={'TopLeft': ' ', 'TopMid': ' ', 'TopRight': ' ', 'MidLeft': ' ', 'MidMid': ' ', 'MidRight': ' ','BotLeft': ' ', 'BotMid': ' ', 'BotRight': ' '}

print('choose any position:  ')
print()
print(' TopLeft | TopMid | TopRight')
print('-------  -------  -------')
print(' MidLeft | MidMid | MidRight')
print('-------  -------  -------')
print(' BotLeft | BotMid | BotRight')
print()
def board(pat):
    print(' ' + pat.get('TopLeft') + '|' + pat.get('TopMid') + ' |' + pat.get('TopRight'))
    print('-- -- --')
    print(' ' + pat.get('MidLeft') + '|' + pat.get('MidMid') + ' |' + pat.get('MidRight'))
    print('-- -- --')
    print(' ' + pat.get('BotLeft') + '|' + pat.get('BotMid') + ' |' + pat.get('BotRight'))
def playerInput(playerNumber):
    
    if playerNumber == 1:
                playerSymbol = 'X'
                player = player1
    else:
                playerSymbol = 'O'
                player = player2
    print(str(player)+ ' : Choose your position ')
    position = input()
    if position not in pat.keys():
                print('This is not a valid position.')
                return playerInput(playerNumber)
    elif pat.get(position) != ' ' :
                print('This position is already taken.')
                return playerInput(playerNumber)
    else:
                pat[position]= playerSymbol
                
def gameBegins():
    i=0
    while ' ' in pat.values():
        if i==1:
            break
        for playerNumber in range(1,3):
            playerInput(playerNumber)
            
            board(pat)
            if(pat.get('TopLeft') == pat.get('TopMid') and pat.get('TopRight') == pat.get('TopMid') and pat.get('TopLeft')!=' ') or (pat.get('MidLeft') == pat.get('MidMid') and pat.get('MidLeft')!=' ' and pat.get('MidRight') == pat.get('MidMid')) or (pat.get('BotLeft') == pat.get('BotMid') and pat.get('BotRight') == pat.get('BotMid')) and pat.get('BotLeft')!=' ' or (pat.get('TopLeft') == pat.get('MidLeft') and pat.get('MidLeft') == pat.get('BotLeft')) and pat.get('TopLeft')!=' ' or (pat.get('TopRight') == pat.get('MidRight') and pat.get('MidRight') == pat.get('BotRight')) and pat.get('TopRight')!=' ' or (pat.get('TopMid') == pat.get('MidMid') and pat.get('MidMid') == pat.get('BotMid')) and pat.get('TopMid')!=' ' or (pat.get('TopLeft') == pat.get('MidMid') and pat.get('MidMid') == pat.get('BotRight')) and pat.get('TopLeft')!=' ' or (pat.get('TopRight') == pat.get('MidMid') and pat.get('BotLeft') == pat.get('MidMid') and pat.get('TopRight')!=' '):
                if playerNumber == 1:
                    player = player1
                else:
                    player = player2
                print(str(player)+ ' wins')
                i=1
                break
            if ' ' in pat.values():
                print('Turn for next player ')
            else:
                print('Its a Draw!!')
                break
gameBegins()
